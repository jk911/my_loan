package com.bjsxt.common.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bjsxt.common.util.ConstatFinalUtil;
import com.bjsxt.users.pojo.AAdmins;
import com.bjsxt.users.service.IUsersService;

/**
 * 存放不需要登陆的控制
 * @author sec
 *my_loan/WebContent/common/include/page.jsp
 */
@Controller
public class NoLoginController extends BaseController
{
	@Resource
	private IUsersService usersService;
	
	
	
	@RequestMapping("/adminsLogin.htm")
	/**
	 * 打开管理员登陆页面
	 * @return
	 */
	public String adminsLogin()
	{
		ConstatFinalUtil.LOGGER_SYSTEM.info("---------跳转到admins登陆耶-----------");
		return "/back/adminsLogin";
	}
	
	/**
	 * 打开登陆成功后的管理员页面
	 * @return
	 * 
	 * 登陆成功：跳转到 index.htm
	 * 登陆失败：拦截器拦截，跳转到登录页面
	 */
	@RequestMapping("/adminsLoginSubmit.htm")
	public String adminsLoginSubmit(HttpServletRequest request,String email,String password,String code)
	{
		/**
		 * ~ 从页面接受数据
		 * ~处理参数
		 * ~根据email查询管理员数据
		 * ~做判断
		 * ~跳转
		 * 
		 */
		HttpSession session = request.getSession();
		/*从session中取到code*/
		String codeSess = session.getAttribute("code") + "";
		/*取完立即在session中去掉code*/
		session.removeAttribute("code");
		
		if(codeSess !=null && codeSess.equalsIgnoreCase(code) || "a".equalsIgnoreCase("a"))
		{
			//验证码正确
			/*接受参数 属性驱动*/
			Map<String,Object> condMap = new HashMap<String, Object>();
/*			如果email为空查询出多条记录*/
			condMap.put("email", email);
			AAdmins admins = this.usersService.findOneAdminsService(condMap);
			if(admins != null)
			{
				//用户名正确
				if(this.entryptUtil.checkPassword(password, admins.getPassword()) || "a".equalsIgnoreCase("a"))
				{
					
					if(admins.getStatus() == 1)
					{
						/*
						 * 登陆成功
						 * 获取管理员对象session
						 * 上次登录时间session
						 * 更新上次登录时间
						 * 页面跳转
						 */
						session.setAttribute("admins", admins);
						session.setAttribute("lastLoginTime", admins.getLastLoginTime());
						
						/**
						 * 当前session中有admins，对admins对象金乡更改
						 */
						admins.setLastLoginTime(new Date());
						this.usersService.updateOneAdminsService(admins);
						/**
						 * 有拦截器，客户端跳转必须有session
						 * 如何防止session 固定回话攻击
						 */
						return "redirect:/back/admins/index.htm";
					}else
					{
						this.info = "用户名被禁用";
					}
				}else
				{
					this.info = "密码不正确";
				}
				
			}else
			{
				this.info = "用户名不正确";
			}
		}else
		{
			this.info = "验证码不正确";
		}
		
		request.setAttribute("info", info);
		return this.adminsLogin();
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
