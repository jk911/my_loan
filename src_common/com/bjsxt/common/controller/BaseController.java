package com.bjsxt.common.controller;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.RequestMapping;

import com.alibaba.fastjson.JSONObject;
import com.bjsxt.common.util.ConstatFinalUtil;
import com.bjsxt.common.util.DateFormatUtil;
import com.bjsxt.common.util.EncryptUtil;
import com.bjsxt.common.util.PageInfoUtil;
import com.bjsxt.common.util.RegexUtil;
import com.bjsxt.common.util.VerifyCodeUtils;

public class BaseController

{
	@Resource
	protected RegexUtil regexUtil;
	
	protected String info;
	@Resource
	protected EncryptUtil entryptUtil;
	@Resource
	protected DateFormatUtil dateFormatUtil;
	
	

	protected JSONObject returnJuiJSON(HttpServletRequest request, String string)
	{
		/* JSON中的其它信息从request中获取,(JSP页面上) */
		String navTabId = request.getParameter("navTabId");
		String rel = request.getParameter("rel");
		String callbackType = request.getParameter("callbackType");
		String forwardUrl = request.getParameter("frowardUrl");
		String confirmMsg = request.getParameter("confirmMsg");
		if(navTabId == null)
		{
			navTabId = "";
		}
		if(rel == null)
		{
			rel = "";
		}
		if(callbackType == null)
		{
			callbackType = "";
		}
		if(forwardUrl == null)
		{
			forwardUrl = "";
		}
		if(confirmMsg == null)
		{
			confirmMsg = "";
		}
		JSONObject resultJSON = new JSONObject();
		resultJSON.put("statusCode", 200);
		resultJSON.put("massage", info);
		resultJSON.put("navTabId", navTabId);
		resultJSON.put("rel", rel);
		resultJSON.put("callbackType", callbackType);
		resultJSON.put("forwardUrl", forwardUrl);
		resultJSON.put("confirmMsg", confirmMsg);
		
		return resultJSON;
	}
	

	/**
	 * 生产验证码
	 * @return
	 */
	@RequestMapping("/code.htm")
	public String code(HttpServletRequest request,HttpServletResponse responce)
	{
		
		HttpSession session = request.getSession();
		ConstatFinalUtil.LOGGER_SYSTEM.info("--------code--------");
		String code = this.regexUtil.ranString(2);
		try
		{
			VerifyCodeUtils.outputImage(75, 24, responce.getOutputStream(), code);
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		session.setAttribute("code", code);
		return null;
		
	}
	/**
	 * 
	 * 生成分页信息
	 * @param request
	 * @return
	 */
	protected PageInfoUtil proccedPageInfo(HttpServletRequest request)
	{
		PageInfoUtil pageInfoUtil = new PageInfoUtil();
		String currentPage = request.getParameter("currentPage");
		String pageSize = request.getParameter("pageSize");
		try
		{
			pageInfoUtil.setCurrentPage(Integer.valueOf(currentPage));
			pageInfoUtil.setPageSize(Integer.valueOf(pageSize));
		} catch (NumberFormatException e)
		{
			/*转换错误  ？？*/
		}
		return pageInfoUtil;
	}
}
