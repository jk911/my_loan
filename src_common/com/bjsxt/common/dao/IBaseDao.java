package com.bjsxt.common.dao;

import java.util.List;
import java.util.Map;

/**
 * 所有查询数据库的基础工具类接口
 * @author sec
 *
 * @param <T>
 */
public interface IBaseDao<T>
{
	/**
	 * 添加一条数据的基础类、
	 * test
	 * @param t
	 * @return
	 */
	int insert(T t);
	/**
	 * 更新一条记录
	 * @param t
	 * @return
	 */
	int update(T t);
	/**
	 * 删除一条 记录
	 * @param t
	 * @return
	 */
	int delete(T t);
	/**
	 * 查询单挑记录
	 * @param condMap
	 * @return
	 */
	T findOne(Map<String,Object> condMap);
	/**
	 * 查询多条记录（分页）
	 * @param condMap
	 * @return
	 */
	List<T> findCondList(Map<String,Object> condMap);
}
