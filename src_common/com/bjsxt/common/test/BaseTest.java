package com.bjsxt.common.test;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.bjsxt.common.util.ConstatFinalUtil;

public class BaseTest
{
	protected ApplicationContext ac;
	
	@Before
	public void init()
	{
		ac = new ClassPathXmlApplicationContext("classpath:spring/applicationContext_*.xml");
		ConstatFinalUtil.LOGGER_SYSTEM.info("--初始化成功--");
	}
	
	@Test
	public void test()
	{
		ConstatFinalUtil.LOGGER_SYSTEM.info("--test--");
	}
}
