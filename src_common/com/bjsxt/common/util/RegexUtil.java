package com.bjsxt.common.util;

import java.util.Random;

import org.springframework.stereotype.Component;

/**
 * 正则表达式工具类
 * 
 * @author sec
 *
 */
@Component("regexUtil")
public class RegexUtil
{
	/*随机字符串*/
	public String randStr = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	/**
	 * 产生随机字符串
	 * @param length
	 * @return
	 */
	public String ranString(int length)
	{
		StringBuffer sb = new StringBuffer();
		Random rand = new Random();
		for(int i = 0 ; i < length ; i ++)
		{
			int randCh = rand.nextInt(this.randStr.length());
			sb.append(this.randStr.charAt(randCh));
		}
		return sb.toString();
	}
}
