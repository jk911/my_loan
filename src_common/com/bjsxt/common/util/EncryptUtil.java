package com.bjsxt.common.util;

import java.security.MessageDigest;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

@Component("encryptUtil")
public class EncryptUtil
{
	@Resource
	private RegexUtil regexUtil = new RegexUtil();

	/**
	 * md5 加密算法模块
	 * 
	 * @param s
	 * @return
	 */
	public String md5(String s)
	{
		char hexDigits[] =
		{ '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
		try
		{
			byte[] btInput = s.getBytes();
			// 获得MD5摘要算法的 MessageDigest 对象
			MessageDigest mdInst = MessageDigest.getInstance("MD5");
			// 使用指定的字节更新摘要
			mdInst.update(btInput);
			// 获得密文
			byte[] md = mdInst.digest();
			// 把密文转换成十六进制的字符串形式
			int j = md.length;
			char str[] = new char[j * 2];
			int k = 0;
			for (int i = 0; i < j; i++)
			{
				byte byte0 = md[i];
				str[k++] = hexDigits[byte0 >>> 4 & 0xf];
				str[k++] = hexDigits[byte0 & 0xf];
			}
			return new String(str);
		} catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 密码字符串
	 * 
	 * 生成后的加密字符串永远不一样
	 * 
	 * @param password
	 * @return
	 */
	public String encryString(String password)
	{
		/*
		 * 明文: 32位随机数$md5$整体加密后的字符串
		 * 
		 * MD5加密:(32位随机数$md5$11111))
		 * 
		 * 密文:32位随机数$md5$ MD5加密:(32位随机数$md5$11111))
		 */
		String randStr = this.regexUtil.ranString(32);
		System.out.println(randStr);
		String md5Str = randStr + "$MD5$";
		System.out.println(md5Str);
		/* 加密字符串 */
		String entryStr = this.md5(md5Str + password);
		System.out.println(entryStr);
		return md5Str + entryStr;

	}

	public boolean checkPassword(String password, String encStr)
	{
		if (encStr.indexOf("$") == -1)
		{
			return false;
		}

		/**
		 * 截取密文中32位的随机字符串和加密方法
		 */
		String[] ranStr = encStr.split("\\$");
		String md5Str = ranStr[0] + "$" + ranStr[1] + "$" + password;
		md5Str = ranStr[0] + "$" + ranStr[1] + "$" + this.md5(md5Str);
	

		return md5Str.equalsIgnoreCase(encStr);

	}
	
	public static void main(String[] args)
	{
		EncryptUtil encryptUtil = new EncryptUtil();
		System.out.println("---" + encryptUtil.md5("111111"));
		System.out.println(encryptUtil.encryString("111111"));
		String souStr = "M7c1v2IkHCxlaTGmgkhyYmvOgmRZPDzf$MD5$7F03227493430FBF835714D19BC44145" ; 
		String pass = "111111" ; 
		System.out.println(encryptUtil.checkPassword(pass, souStr));
	}

}
