package com.bjsxt.common.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.stereotype.Component;

/**
 * 日期工具类
 * @author wangshSxt
 *
 */
@Component("dateFormatUtil")
public class DateFormatUtil
{
	/* 从配置文件中读取日期的格式 */
	private String defaultDateFormat = ConstatFinalUtil.BUNDLE.getString("format.date");
	private String defaultDateTimeFormat = ConstatFinalUtil.BUNDLE.getString("format.dateTime");
	
	/**
	 * 日期格式化
	 * 将日期转换为字符串
	 * @return
	 */
	public String format(Date date,String pattern)
	{
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		return sdf.format(date);
	}
	
	/**
	 * 日期格式化
	 * 将日期转换为字符串
	 * @return
	 */
	public String formatDate(Date date)
	{
		SimpleDateFormat sdf = new SimpleDateFormat(defaultDateFormat);
		return sdf.format(date);
	}
	
	/**
	 * 将字符串解析为日期时间类型
	 * @param source
	 * @return
	 */
	public Date parseDateTime(String source)
	{
		SimpleDateFormat sdf = new SimpleDateFormat(defaultDateTimeFormat);
		try
		{
			return sdf.parse(source);
		} catch (ParseException e)
		{
		}
		return null ;
	}
	
	/**
	 * 将字符串解析为日期时间类型
	 * @param source
	 * @return
	 */
	public Date parseDate(String source)
	{
		SimpleDateFormat sdf = new SimpleDateFormat(defaultDateFormat);
		try
		{
			return sdf.parse(source);
		} catch (ParseException e)
		{
		}
		return null ;
	}
	
	/**
	 * 将字符串解析为日期时间类型,
	 * 自定义格式
	 * @param source
	 * @return
	 */
	public Date parse(String source,String pattern)
	{
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		try
		{
			return sdf.parse(source);
		} catch (ParseException e)
		{
		}
		return null ;
	}
}

