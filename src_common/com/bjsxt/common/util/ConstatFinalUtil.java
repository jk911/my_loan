package com.bjsxt.common.util;

import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * 放的一些常量信息
 * @author wangshSxt
 *
 */
public class ConstatFinalUtil
{
	/* 系统的Logger对象 */
	public static final Logger LOGGER = LogManager.getLogger(ConstatFinalUtil.class);
	/* 自定义的LOgger对象 */
	public static final Logger LOGGER_SYSTEM = LogManager.getLogger("System");
	public static final ResourceBundle BUNDLE = ResourceBundle.getBundle("loan");
}
