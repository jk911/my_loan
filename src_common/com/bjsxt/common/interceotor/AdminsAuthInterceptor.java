package com.bjsxt.common.interceotor;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.bjsxt.users.pojo.AAdmins;

@Component("adminsAuthInterceptor")
public class AdminsAuthInterceptor extends HandlerInterceptorAdapter 
{
	@Override
	public boolean preHandle(HttpServletRequest request,HttpServletResponse response, Object handler) throws Exception
	{
		
		/**
		 * 从session中获取admins对象
		 * 判断是否为空
		 */
		HttpSession session = request.getSession();
		AAdmins admins = (AAdmins) session.getAttribute("admins");
		if(admins != null)
		{
			/*已经登陆*/
			return true;
		}
		String info = "请先登录" ;
		session.setAttribute("info", info);
		response.sendRedirect(request.getContextPath() + "/adminsLogin.htm");
		return false;
	}
}
