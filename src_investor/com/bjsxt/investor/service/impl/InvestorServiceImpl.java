package com.bjsxt.investor.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.bjsxt.common.util.PageInfoUtil;
import com.bjsxt.investor.dao.IAGrantDao;
import com.bjsxt.investor.dao.IAInvestorBankDao;
import com.bjsxt.investor.dao.IAInvestorDao;
import com.bjsxt.investor.dao.IAInvestorGrantDao;
import com.bjsxt.investor.pojo.AGrant;
import com.bjsxt.investor.pojo.AInvestor;
import com.bjsxt.investor.pojo.AInvestorBank;
import com.bjsxt.investor.pojo.AInvestorGrant;
import com.bjsxt.investor.service.IInvestorService;

@Service("investorService")
public class InvestorServiceImpl implements IInvestorService
{
	@Resource
	private IAGrantDao grantDao;
	@Resource
	private IAInvestorDao      investorDao;
	@Resource				  
	private IAInvestorBankDao  investorBankDao;
	@Resource
	private IAInvestorGrantDao investorGrantDao;

	@Override
	public JSONObject insertOneGrantService(AGrant grant)
	{
		JSONObject resultJSON = new JSONObject();
		int res = this.grantDao.insert(grant);
		if(res > 0)
		{
			resultJSON.put("code", "0");
			resultJSON.put("info", "添加成功");
			JSONObject dataJSON = new JSONObject();
			dataJSON.put("id", grant.getId());
			resultJSON.put("data", dataJSON);
			
		}else
		{
			resultJSON.put("code", "1");
			resultJSON.put("info", "添加失败");
		}
		
		return resultJSON;
	}

	@Override
	public JSONObject updateOneGrantService(AGrant grant)
	{
		JSONObject resultJSON = new JSONObject();
		int res = this.grantDao.insert(grant);
		if(res > 0)
		{
			resultJSON.put("code", "0");
			resultJSON.put("info", "更新成功");
			JSONObject dataJSON = new JSONObject();
			dataJSON.put("id", grant.getId());
			resultJSON.put("data", dataJSON);
			
		}else
		{
			resultJSON.put("code", "1");
			resultJSON.put("info", "更新失败");
		}
		
		return resultJSON;
	}

	@Override
	public AGrant findOneGrantService(Map<String, Object> condMap)
	{
		// TODO Auto-generated method stub
		return this.grantDao.findOne(condMap);
	}

	@Override
	public List<AGrant> findCondListGrantService(PageInfoUtil pageInfoUtil, Map<String, Object> condMap)
	{
		
		List<AGrant> grantList = new ArrayList<AGrant>();
		/*处理关键字*/
		if(condMap.get("keyword") != null)
		{
			condMap.put("keyword", "%"+ condMap.get("keyword")+"%");
			
		}
		/*查询总记录数*/
		if(pageInfoUtil != null)
		{
			condMap.put("pageCount", "false");
			/*查询总记录数*/
			grantList = this.grantDao.findCondList(condMap);
			/*grantList有一条数据，放在id上*/
			pageInfoUtil.setTotalRecord(grantList.get(0).getId());
			condMap.put("pageCount", "false");
			/*当前页面和页面size在pageInfoutil中拿*/
			condMap.put("currentRecord",pageInfoUtil.getCurrRecord());
			condMap.put("pageSize", pageInfoUtil.getPageSize());
			grantList = this.grantDao.findCondList(condMap);
		}else
		{
			//不分页查询
			grantList = this.grantDao.findCondList(condMap);
		}
		
		return grantList;
	}

	@Override
	public JSONObject insertOneInvestorService(AInvestor investor)
	{
		JSONObject resultJSON = new JSONObject();
		int res = this.investorDao.insert(investor);
		if(res > 0)
		{
			resultJSON.put("code", "0");
			resultJSON.put("info", "添加成功");
			JSONObject dataJSON = new JSONObject();
			dataJSON.put("id", investor.getId());
			resultJSON.put("data", dataJSON);
			
		}else
		{
			resultJSON.put("code", "1");
			resultJSON.put("info", "添加失败");
		}
		
		return resultJSON;
	}

	@Override
	public JSONObject updateOneInvestorService(AInvestor investor)
	{
		JSONObject resultJSON = new JSONObject();
		int res = this.investorDao.insert(investor);
		if(res > 0)
		{
			resultJSON.put("code", "0");
			resultJSON.put("info", "更新成功");
			JSONObject dataJSON = new JSONObject();
			dataJSON.put("id", investor.getId());
			resultJSON.put("data", dataJSON);
			
		}else
		{
			resultJSON.put("code", "1");
			resultJSON.put("info", "更新失败");
		}
		
		return resultJSON;
	}

	@Override
	public AInvestor findOneInvestorService(Map<String, Object> condMap)
	{
		// TODO Auto-generated method stub
		return this.investorDao.findOne(condMap);
	}

	@Override
	public List<AInvestor> findCondListInvestorService(PageInfoUtil pageInfoUtil, Map<String, Object> condMap)
	{
		List<AInvestor> investorList = new ArrayList<AInvestor>();
		/*处理字符串
		 * 	关键字 like查询
		 * */
		if(condMap.get("keyword") != null)
		{
			condMap.put("keyword","%" + condMap.get("keyword") + "%");
		}
		
		/*处理分页*/
		
		if(pageInfoUtil != null)
		{
			condMap.put("pageCount", "true");
			investorList = this.investorDao.findCondList(condMap);
			pageInfoUtil.setTotalRecord(investorList.get(0).getId());
			condMap.put("pageCount", "false");
			condMap.put("currentRecord", pageInfoUtil.getCurrRecord());
			condMap.put("pageSize", pageInfoUtil.getPageSize());
			investorList = this.investorDao.findCondList(condMap);
		}else
		{
			investorList = this.investorDao.findCondList(condMap);
		}
		
		return investorList;
	}

	@Override
	public JSONObject insertOneInvestorGrantService(AInvestorGrant investorGrant)
	{
		JSONObject resultJSON = new JSONObject();
		int res = this.investorGrantDao.insert(investorGrant);
		if(res > 0)
		{
			resultJSON.put("code", "0");
			resultJSON.put("info", "添加成功");
			JSONObject dataJSON = new JSONObject();
			dataJSON.put("id", investorGrant.getId());
			resultJSON.put("data", dataJSON);
			
		}else
		{
			resultJSON.put("code", "1");
			resultJSON.put("info", "添加失败");
		}
		
		return resultJSON;
	}

	@Override
	public JSONObject updateOneInvestorGrantService(AInvestorGrant investorGrant)
	{
		JSONObject resultJSON = new JSONObject();
		int res = this.investorGrantDao.insert(investorGrant);
		if(res > 0)
		{
			resultJSON.put("code", "0");
			resultJSON.put("info", "更新成功");
			JSONObject dataJSON = new JSONObject();
			dataJSON.put("id", investorGrant.getId());
			resultJSON.put("data", dataJSON);
			
		}else
		{
			resultJSON.put("code", "1");
			resultJSON.put("info", "更新失败");
		}
		
		return resultJSON;
	}

	@Override
	public AInvestorGrant findOneInvestorGrantService(Map<String, Object> condMap)
	{
		// TODO Auto-generated method stub
		return this.investorGrantDao.findOne(condMap);
	}

	@Override
	public List<AInvestorGrant> findCondListInvestorGrantService(PageInfoUtil pageInfoUtil, Map<String, Object> condMap)
	{
		List<AInvestorGrant> investorGrantList = new ArrayList<AInvestorGrant>();
		if(condMap.get("keyword") != null)
		{
			condMap.put("keyword","%" + condMap.get("keyword") + "%"); 
		}
		
		if(pageInfoUtil != null)
		{
			condMap.put("pageCount", "true");
			investorGrantList = this.investorGrantDao.findCondList(condMap);
			condMap.put("pageCount", "false" );
			pageInfoUtil.setTotalRecord(investorGrantList.get(0).getId());
			condMap.put("currentRecord", pageInfoUtil.getCurrRecord());
			condMap.put("pageSize", pageInfoUtil.getPageSize());
			investorGrantList = this.investorGrantDao.findCondList(condMap);
		}else
		{
			investorGrantList = this.investorGrantDao.findCondList(condMap);
		}
		return investorGrantList;
	}

	
	
	
	@Override
	public AInvestorBank findOneInvestorBankService(Map<String, Object> condMap)
	{
		return this.investorBankDao.findOne(condMap);
	}

	@Override
	public JSONObject insertOneInvestorBankService(AInvestorBank investorBank)
	{
		JSONObject resultJSON = new JSONObject();
		
		int res = this.investorBankDao.insert(investorBank);
		/*String str = null ; 
		str.toString() ; */
		
		if(res > 0)
		{
			resultJSON.put("code", "0");
			resultJSON.put("info", "添加成功");
			
			/* 二级结构 */
			JSONObject dataJSON = new JSONObject();
			dataJSON.put("id", investorBank.getId());
			resultJSON.put("data", dataJSON);
		}else
		{
			resultJSON.put("code", "1");
			resultJSON.put("info", "添加失败");
		}
		return resultJSON;
	}

	@Override
	public JSONObject updateOneInvestorBankService(AInvestorBank investorBank)
	{
		JSONObject resultJSON = new JSONObject();
		int res = this.investorBankDao.update(investorBank);
		
		/*String str = null ; 
		str.toString() ; */
		
		if(res > 0)
		{
			resultJSON.put("code", "0");
			resultJSON.put("info", "更新成功");
			
		}else
		{
			resultJSON.put("code", "1");
			resultJSON.put("info", "更新失败");
		}
		return resultJSON;
	}

	@Override
	public List<AInvestorBank> findCondListInvestorBankService(PageInfoUtil pageInfoUtil, Map<String, Object> condMap)
	{
		List<AInvestorBank> investorBankList = new ArrayList<AInvestorBank>();
		/*
		 * 处理一下关键字
		 * keyword 变成: % + keyword + %
		 * */
		if(condMap.get("keyword") != null)
		{
			condMap.put("keyword", "%" + condMap.get("keyword") + "%");
		}
		
		/*pageCount == true:拿总记录数:
		pageCount == false:分页数据:
		pageCount 为null不分页查询*/
		if(pageInfoUtil != null)
		{
			//分页查询
			/* 先查询总记录数 */
			condMap.put("pageCount", "true");
			investorBankList = this.investorBankDao.findCondList(condMap);
			/* investorBankList有一条记录,总记录数放到了id属性上 */
			pageInfoUtil.setTotalRecord(investorBankList.get(0).getId());
			
			/* 拿分页数据 */
			condMap.put("pageCount", "false");
			/* 当前记录数
			 * 每页多少条 */
			condMap.put("currentRecord", pageInfoUtil.getCurrRecord());
			condMap.put("pageSize", pageInfoUtil.getPageSize());
			investorBankList = this.investorBankDao.findCondList(condMap);
		}else
		{
			//不分页查询
			investorBankList = this.investorBankDao.findCondList(condMap);
		}
		return investorBankList;
	}
}
