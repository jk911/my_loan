package com.bjsxt.investor.service;

import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.bjsxt.common.util.PageInfoUtil;
import com.bjsxt.investor.pojo.AGrant;
import com.bjsxt.investor.pojo.AInvestor;
import com.bjsxt.investor.pojo.AInvestorBank;
import com.bjsxt.investor.pojo.AInvestorGrant;

	/**
	 * 出资方相关的Service
	 * @author sec
	 */
	public interface IInvestorService
	{
		/* ---- 授权操作开始 ---- */
		/**
		 * 添加一条授权
		 * 
		 * @param grant
		 * @return
		 */
		JSONObject insertOneGrantService(AGrant grant);
		
		/**
		 * 更新一条授权
		 * 
		 * @param grant
		 * @return
		 */
		JSONObject updateOneGrantService(AGrant grant);

		/**
		 * 查询单条授权
		 * 
		 * @param condMap
		 * @return
		 */
		AGrant findOneGrantService(Map<String, Object> condMap);
		
		/**
		 * 查询多条授权
		 * @param pageInfoUtil 分页对象
		 * @param condMap	条件
		 * @return
		 */
		List<AGrant> findCondListGrantService(PageInfoUtil pageInfoUtil ,Map<String, Object> condMap);
		/* ---- 授权操作结束 ----- */
		
		/* ---- 出资方操作开始 ---- */
		/**
		 * 添加一条出资方
		 * 
		 * @param investor
		 * @return
		 */
		JSONObject insertOneInvestorService(AInvestor investor);
		
		/**
		 * 更新一条出资方
		 * 
		 * @param investor
		 * @return
		 */
		JSONObject updateOneInvestorService(AInvestor investor);

		/**
		 * 查询单条出资方
		 * 
		 * @param condMap
		 * @return
		 */
		AInvestor findOneInvestorService(Map<String, Object> condMap);
		
		/**
		 * 查询多条出资方
		 * @param pageInfoUtil 分页对象
		 * @param condMap	条件
		 * @return
		 */
		List<AInvestor> findCondListInvestorService(PageInfoUtil pageInfoUtil ,Map<String, Object> condMap);
		/* ---- 出资方操作结束 ----- */
		
		/* ---- 出资方授权操作开始 ---- */
		/**
		 * 添加一条出资方授权
		 * 
		 * @param investorGrant
		 * @return
		 */
		JSONObject insertOneInvestorGrantService(AInvestorGrant investorGrant);
		
		/**
		 * 更新一条出资方授权
		 * 
		 * @param investorGrant
		 * @return
		 */
		JSONObject updateOneInvestorGrantService(AInvestorGrant investorGrant);

		/**
		 * 查询单条出资方授权
		 * 
		 * @param condMap
		 * @return
		 */
		AInvestorGrant findOneInvestorGrantService(Map<String, Object> condMap);
		
		/**
		 * 查询多条出资方授权
		 * @param pageInfoUtil 分页对象
		 * @param condMap	条件
		 * @return
		 */
		List<AInvestorGrant> findCondListInvestorGrantService(PageInfoUtil pageInfoUtil ,Map<String, Object> condMap);
		/* ---- 出资方授权操作结束 ----- */
		
		/* ---- 出资方银行操作开始 ---- */
		/**
		 * 添加一条出资方银行
		 * 
		 * @param InvestorBank
		 * @return
		 */
		JSONObject insertOneInvestorBankService(AInvestorBank InvestorBank);
		
		/**
		 * 更新一条出资方银行
		 * 
		 * @param InvestorBank
		 * @return
		 */
		JSONObject updateOneInvestorBankService(AInvestorBank InvestorBank);

		/**
		 * 查询单条出资方银行
		 * 
		 * @param condMap
		 * @return
		 */
		AInvestorBank findOneInvestorBankService(Map<String, Object> condMap);
		
		/**
		 * 查询多条出资方银行
		 * @param pageInfoUtil 分页对象
		 * @param condMap	条件
		 * @return
		 */
		List<AInvestorBank> findCondListInvestorBankService(PageInfoUtil pageInfoUtil ,Map<String, Object> condMap);
		/* ---- 出资方银行操作结束 ----- */
}
