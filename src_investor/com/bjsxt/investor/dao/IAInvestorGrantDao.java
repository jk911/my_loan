package com.bjsxt.investor.dao;

import com.bjsxt.common.dao.IBaseDao;
import com.bjsxt.investor.pojo.AInvestorGrant;

public interface IAInvestorGrantDao extends IBaseDao<AInvestorGrant>
{
	
}
