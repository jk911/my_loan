package com.bjsxt.investor.dao;

import com.bjsxt.common.dao.IBaseDao;
import com.bjsxt.investor.pojo.AInvestorBank;

public interface IAInvestorBankDao extends IBaseDao<AInvestorBank>
{
	
}
