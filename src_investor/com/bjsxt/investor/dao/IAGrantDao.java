package com.bjsxt.investor.dao;

import com.bjsxt.common.dao.IBaseDao;
import com.bjsxt.investor.pojo.AGrant;

public interface IAGrantDao extends IBaseDao<AGrant>
{
	
}
