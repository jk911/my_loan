package com.bjsxt.investor.pojo;

import java.util.Date;

/**
 * 出次方银行的POJO
 * 
 * @author wangshSxt
 *
 */
public class AInvestorBank
{
	private int id;
	private int investorId;
	private int bankId;
	private byte status;
	private Date createTime;
	private Date updateTime;
	private Date pubTime;

	/* 字符串描述 */
	private String statusStr;

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public int getInvestorId()
	{
		return investorId;
	}

	public void setInvestorId(int investorId)
	{
		this.investorId = investorId;
	}

	public int getBankId()
	{
		return bankId;
	}

	public void setBankId(int bankId)
	{
		this.bankId = bankId;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public Date getCreateTime()
	{
		return createTime;
	}

	public void setCreateTime(Date createTime)
	{
		this.createTime = createTime;
	}

	public Date getUpdateTime()
	{
		return updateTime;
	}

	public void setUpdateTime(Date updateTime)
	{
		this.updateTime = updateTime;
	}

	public Date getPubTime()
	{
		return pubTime;
	}

	public void setPubTime(Date pubTime)
	{
		this.pubTime = pubTime;
	}

	public String getStatusStr()
	{
		/*
		 * 状态:0:禁用:1:启用
		 */
		if (this.status == 0)
		{
			this.statusStr = "禁用";
		} else if (this.status == 1)
		{
			this.statusStr = "启用";
		}
		return statusStr;
	}


}