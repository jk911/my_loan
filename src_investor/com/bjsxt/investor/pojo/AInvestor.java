package com.bjsxt.investor.pojo;

import java.util.Date;
/**
 * 授权的pojo
 * @author sec
 *
 */
public class AInvestor
{
	private int id;
	private String code;
	private String name;
	private String url;
	private String contactName;
	private String contactPhone;
	private String logo;
	private String content;
	private int totalNum;
	private int succedNum;
	private byte coopType;
	private byte status;
	private Date createTime;
	private Date updateTime;
	private Date pubTime;

	/* 字符串描述 */
	private String statusStr;
	private String coopTypeStr;

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getCode()
	{
		return code;
	}

	public void setCode(String code)
	{
		this.code = code;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getUrl()
	{
		return url;
	}

	public void setUrl(String url)
	{
		this.url = url;
	}

	public String getContactName()
	{
		return contactName;
	}

	public void setContactName(String contactName)
	{
		this.contactName = contactName;
	}

	public String getContactPhone()
	{
		return contactPhone;
	}

	public void setContactPhone(String contactPhone)
	{
		this.contactPhone = contactPhone;
	}

	public String getLogo()
	{
		return logo;
	}

	public void setLogo(String logo)
	{
		this.logo = logo;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public int getTotalNum()
	{
		return totalNum;
	}

	public void setTotalNum(int totalNum)
	{
		this.totalNum = totalNum;
	}

	public int getSuccedNum()
	{
		return succedNum;
	}

	public void setSuccedNum(int succedNum)
	{
		this.succedNum = succedNum;
	}

	public byte getCoopType()
	{
		return coopType;
	}

	public void setCoopType(byte coopType)
	{
		this.coopType = coopType;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public Date getCreateTime()
	{
		return createTime;
	}

	public void setCreateTime(Date createTime)
	{
		this.createTime = createTime;
	}

	public Date getUpdateTime()
	{
		return updateTime;
	}

	public void setUpdateTime(Date updateTime)
	{
		this.updateTime = updateTime;
	}

	public Date getPubTime()
	{
		return pubTime;
	}

	public void setPubTime(Date pubTime)
	{
		this.pubTime = pubTime;
	}

	public String getStatusStr()
	{
		/*
		 * 状态:0:禁用:1:启用
		 */
		if (this.status == 0)
		{
			this.statusStr = "禁用";
		} else if (this.status == 1)
		{
			this.statusStr = "启用";
		}
		return statusStr;
	}

	public String getCoopTypeStr()
	{
		if (this.coopType == 0)
		{
			this.coopTypeStr = "直通";
		} else if (this.coopType == 1)
		{
			this.coopTypeStr = "非直通";
		}
		return coopTypeStr;
	}

}