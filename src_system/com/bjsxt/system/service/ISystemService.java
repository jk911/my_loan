package com.bjsxt.system.service;

import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.bjsxt.common.util.PageInfoUtil;
import com.bjsxt.system.pojo.ACate;
import com.bjsxt.system.pojo.ARegion;

public interface ISystemService 
{
	/* ---- 地区操作开始 ---- */
	/**
	 * 添加一条地区
	 * 
	 * @param region
	 * @return
	 */
	JSONObject insertOneRegionService(ARegion region);
	
	/**
	 * 更新一条地区
	 * 
	 * @param region
	 * @return
	 */
	JSONObject updateOneRegionService(ARegion region);

	/**
	 * 查询单条地区
	 * 
	 * @param condMap
	 * @return
	 */
	ARegion findOneRegionService(Map<String, Object> condMap);
	
	/**
	 * 查询多条地区
	 * @param pageInfoUtil 分页对象
	 * @param condMap	条件
	 * @return
	 */
	List<ARegion> findCondListRegionService(PageInfoUtil pageInfoUtil ,Map<String, Object> condMap);
	/* ---- 地区操作结束 ----- */
	
	/* ---- 分类操作开始 ---- */
	/**
	 * 添加一条分类
	 * 
	 * @param cate
	 * @return
	 */
	JSONObject insertOneCateService(ACate cate);
	
	/**
	 * 更新一条分类
	 * 
	 * @param cate
	 * @return
	 */
	JSONObject updateOneCateService(ACate cate);

	/**
	 * 查询单条分类
	 * 
	 * @param condMap
	 * @return
	 */
	ACate findOneCateService(Map<String, Object> condMap);
	
	/**
	 * 查询多条分类
	 * @param pageInfoUtil 分页对象
	 * @param condMap	条件
	 * @return
	 */
	List<ACate> findCondListCateService(PageInfoUtil pageInfoUtil ,Map<String, Object> condMap);
	/* ---- 分类操作结束 ----- */
}
