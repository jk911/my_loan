package com.bjsxt.system.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.bjsxt.common.util.PageInfoUtil;
import com.bjsxt.system.dao.IACateDao;
import com.bjsxt.system.dao.IARegionDao;
import com.bjsxt.system.pojo.ACate;
import com.bjsxt.system.pojo.ARegion;
import com.bjsxt.system.service.ISystemService;

import javassist.compiler.ast.Keyword;

@Service("systemService")
public class SystemServiceImpl implements ISystemService
{
	@Resource
	private IARegionDao regionDao;
	@Resource
	private IACateDao cateDao;
	@Override
	public JSONObject insertOneRegionService(ARegion region)
	{
		JSONObject resultJSON = new JSONObject();
		int res = this.regionDao.insert(region);
		if(res > 0)
		{
			resultJSON.put("code", "0");
			resultJSON.put("info", "添加成功");
			/*返回主键*/
			JSONObject dataJSON = new JSONObject();
			dataJSON.put("id",region.getId());
			resultJSON.put("data", dataJSON);
		}else
		{
			resultJSON.put("code", "1");
			resultJSON.put("info", "添加失败");
		}
		return resultJSON;
	}

	@Override
	public JSONObject updateOneRegionService(ARegion region)
	{
		JSONObject resultJSON = new JSONObject();
		int res = this.regionDao.update(region);
		if(res > 0)
		{
			resultJSON.put("code", "0");
			resultJSON.put("info", "更新成功");
			
		}else
		{
			resultJSON.put("code", "1");
			resultJSON.put("info", "更新失败");
		}
		return resultJSON;
	}

	@Override
	public ARegion findOneRegionService(Map<String, Object> condMap)
	{
		// TODO Auto-generated method stub
		return this.regionDao.findOne(condMap);
	}

	@Override
	public List<ARegion> findCondListRegionService(PageInfoUtil pageInfoUtil, Map<String, Object> condMap)
	{
		List<ARegion> regionList = new ArrayList<ARegion>();
		/**
		 * 处理一下关键字
		 * 在前后加%%
		 */
		if(condMap.get("keyword") != null )
		{
			condMap.put("keyword", "%"+condMap.get("keyword")+"%");
		}
		/*pageCount == true:拿总记录数:
		pageCount == false:分页数据:
		pageCount 为null不分页查询*/
		if(pageInfoUtil != null)
		{
			/*分页查询
			 * 先查总记录数
			 * 如果pageinfoUtil不为空，那么就把pageCount置为true
			 * */
			condMap.put("pageCount","true");
			/*查询总记录数*/
			regionList = this.regionDao.findCondList(condMap);
			/**
			 * 设置分页
			 * 吧分页信息传进pageInfoUtil中
			 */
			pageInfoUtil.setTotalRecord(regionList.get(0).getId());
			/*page信息中有了分页参数*/
			/**
			 *此时pageCount已经使用了查询总数，在查询分页的时候置为false
			 */
			condMap.put("pageCount", "false");
			condMap.put("currentRecord", pageInfoUtil.getCurrRecord());
			condMap.put("pageSize", pageInfoUtil.getPageSize());
			regionList = this.regionDao.findCondList(condMap);
		}else
		{
			regionList = this.regionDao.findCondList(condMap);
		}
		return regionList;
	}

	@Override
	public JSONObject insertOneCateService(ACate cate)
	{
		JSONObject resultJSON = new JSONObject();
		int res = this.cateDao.insert(cate);
		if(res > 0)
		{
			resultJSON.put("code", "0");
			resultJSON.put("info", "添加成功");
			/*返回主键*/
			JSONObject dataJSON = new JSONObject();
			dataJSON.put("id",cate.getId());
			resultJSON.put("data", dataJSON);
		}else
		{
			resultJSON.put("code", "1");
			resultJSON.put("info", "添加失败");
		}
		return resultJSON;
	}

	@Override
	public JSONObject updateOneCateService(ACate cate)
	{
		JSONObject resultJSON = new JSONObject();
		int res = this.cateDao.update(cate);
		if(res > 0)
		{
			resultJSON.put("code", "0");
			resultJSON.put("info", "更新成功");
			
		}else
		{
			resultJSON.put("code", "1");
			resultJSON.put("info", "更新失败");
		}
		return resultJSON;
	}

	@Override
	public ACate findOneCateService(Map<String, Object> condMap)
	{
		// TODO Auto-generated method stub
		return this.cateDao.findOne(condMap);
	}

	@Override
	public List<ACate> findCondListCateService(PageInfoUtil pageInfoUtil, Map<String, Object> condMap)
	{
		List<ACate> cateList = new ArrayList<ACate>();
		/**
		 * 处理一下关键字
		 * 在前后加%%
		 */
		if(condMap.get("keyword") != null )
		{
			condMap.put("keyword", "%"+condMap.get("keyword")+"%");
		}
		/*pageCount == true:拿总记录数:
		pageCount == false:分页数据:
		pageCount 为null不分页查询*/
		if(pageInfoUtil != null)
		{
			/*分页查询
			 * 先查总记录数
			 * 如果pageinfoUtil不为空，那么就把pageCount置为true
			 * */
			condMap.put("pageCount","true");
			/*查询总记录数*/
			cateList = this.cateDao.findCondList(condMap);
			/**
			 * 设置分页
			 * 吧分页信息传进pageInfoUtil中
			 */
			pageInfoUtil.setTotalRecord(cateList.get(0).getId());
			/*page信息中有了分页参数*/
			/**
			 *此时pageCount已经使用了查询总数，在查询分页的时候置为false
			 */
			condMap.put("pageCount", false);
			condMap.put("currentRecord", pageInfoUtil.getCurrRecord());
			condMap.put("pageSize", pageInfoUtil.getPageSize());
			cateList = this.cateDao.findCondList(condMap);
		}else
		{
			cateList = this.cateDao.findCondList(condMap);
		}
		return cateList;
	}
}
