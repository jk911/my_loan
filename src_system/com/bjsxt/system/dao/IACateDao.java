package com.bjsxt.system.dao;

import org.springframework.stereotype.Service;

import com.bjsxt.common.dao.IBaseDao;
import com.bjsxt.system.pojo.ACate;

public interface IACateDao extends IBaseDao<ACate>
{

}
