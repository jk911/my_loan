package com.bjsxt.system.dao;

import org.springframework.stereotype.Service;

import com.bjsxt.common.dao.IBaseDao;
import com.bjsxt.system.pojo.ARegion;

public interface IARegionDao extends IBaseDao<ARegion>
{

}
