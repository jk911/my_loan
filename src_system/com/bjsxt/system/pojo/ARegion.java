package com.bjsxt.system.pojo;

import java.util.Date;
import java.util.Set;
import java.util.TreeSet;

public class ARegion
{
	private int id;
	private int parentId;
	private String name;
	private String pinyin;
	private String areacode;
	private String content;
	private byte leafStatus;
	private byte status;
	private Date createTime;
	private Date updateTime;
	private Date pubTime;
	
	/*配置关联对象*/
	private ARegion parentRegion;
	private Set<ARegion> childrenRegionSet = new TreeSet<>();
	
	/*字符串描述*/
	private String statusStr;
	private String leafStatusStr;
	
	
	
	
	
	public int getId()
	{
		return id;
	}
	public void setId(int id)
	{
		this.id = id;
	}
	public int getParentId()
	{
		return parentId;
	}
	public void setParentId(int parentId)
	{
		this.parentId = parentId;
	}
	public String getName()
	{
		return name;
	}
	public void setName(String name)
	{
		this.name = name;
	}
	public String getPinyin()
	{
		return pinyin;
	}
	public void setPinyin(String pinyin)
	{
		this.pinyin = pinyin;
	}
	public String getAreacode()
	{
		return areacode;
	}
	public void setAreacode(String areacode)
	{
		this.areacode = areacode;
	}
	public String getContent()
	{
		return content;
	}
	public void setContent(String content)
	{
		this.content = content;
	}
	public byte getLeafStatus()
	{
		return leafStatus;
	}
	public void setLeafStatus(byte leafStatus)
	{
		this.leafStatus = leafStatus;
	}
	public byte getStatus()
	{
		return status;
	}
	public void setStatus(byte status)
	{
		this.status = status;
	}
	public Date getCreateTime()
	{
		return createTime;
	}
	public void setCreateTime(Date createTime)
	{
		this.createTime = createTime;
	}
	public Date getUpdateTime()
	{
		return updateTime;
	}
	public void setUpdateTime(Date updateTime)
	{
		this.updateTime = updateTime;
	}
	public Date getPubTime()
	{
		return pubTime;
	}
	public void setPubTime(Date pubTime)
	{
		
		this.pubTime = pubTime;
	}
	public String getStatusStr()
	{
		if(this.status == 0)
		{
			this.statusStr = "禁用" ;
		}else if(this.status == 1)
		{
			this.statusStr = "启用"; 
		}
		return "未知";
	}
	public String getLeafStatusStr()
	{
		if(this.leafStatus == 0)
		{
			this.leafStatusStr = "非叶子节点" ;
		}else if(this.leafStatus == 1)
		{
			this.leafStatusStr = "叶子节点"; 
		}
		return "异常";
	}
	public ARegion getParentRegion()
	{
		return parentRegion;
	}
	public void setParentRegion(ARegion parentRegion)
	{
		this.parentRegion = parentRegion;
	}
	public Set<ARegion> getChildrenRegionSet()
	{
		return childrenRegionSet;
	}
	public void setChildrenRegionSet(Set<ARegion> childrenRegionSet)
	{
		this.childrenRegionSet = childrenRegionSet;
	}
}
