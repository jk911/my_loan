package com.bjsxt.system.test;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.alibaba.fastjson.JSONObject;
import com.bjsxt.common.test.BaseTest;
import com.bjsxt.common.util.ConstatFinalUtil;
import com.bjsxt.system.pojo.ARegion;
import com.bjsxt.system.service.ISystemService;
import com.bjsxt.system.service.impl.SystemServiceImpl;

public class SystemServiceTest extends BaseTest
{
	
	private ISystemService systemService ;
	
	@Before
	public void init()
	{
		super.init();
		systemService = (ISystemService) this.ac.getBean("systemService");
	}
	@Test
	public void test()
	{
		ConstatFinalUtil.LOGGER.info("---------systemService 初始化耶---------");
	}

	/**
	 * 查询一条记录
	 */
	@Test
	public void findOneRegionService()
	{
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", "1729");
		ARegion region = this.systemService.findOneRegionService(condMap );
		ConstatFinalUtil.LOGGER_SYSTEM.info("id:{},名称:{};areacode:{}",region.getId() , 
				region.getName(),
				region.getAreacode());
		ARegion parentRegion = region.getParentRegion() ; 
		ConstatFinalUtil.LOGGER_SYSTEM.info("父类:id:{},名称:{};areacode:{}",parentRegion.getId() , 
				parentRegion.getName(),
				parentRegion.getAreacode());
		
		for (Iterator<?> iterator = region.getChildrenRegionSet().iterator(); iterator.hasNext();)
		{
			ARegion childRegion = (ARegion) iterator.next();
			ConstatFinalUtil.LOGGER_SYSTEM.info("子类:id:{},名称:{};areacode:{}",childRegion.getId() , 
					childRegion.getName(),
					childRegion.getAreacode());
		}
	}
	
	/**
	 * 添加一条地区
	 */
	@Test
	public void insertOneRegionService()
	{
		ARegion region = new ARegion();
		region.setName("22");
		region.setParentId(0);
		region.setPinyin("22");
		region.setAreacode("222");
		region.setContent("222");
		region.setCreateTime(new Date());
		region.setUpdateTime(new Date());
		region.setPubTime(new Date());
		JSONObject resultJSON = this.systemService.insertOneRegionService(region);
		ConstatFinalUtil.LOGGER_SYSTEM.info("返回信息:{}",resultJSON);
	}
	
	/**
	 * 查询多条记录
	 */
	@Test
	public void findCondListRegionService()
	{
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("parentId", "0");
		
		List<ARegion> regionList = this.systemService.findCondListRegionService(null, condMap);
		for (Iterator iterator = regionList.iterator(); iterator.hasNext();)
		{
			ARegion region = (ARegion) iterator.next();
			ConstatFinalUtil.LOGGER_SYSTEM.info("地区:id:{},名称:{};areacode:{}",region.getId() , 
					region.getName(),
					region.getAreacode());
		}
	}
}
