package com.bjsxt.users.pojo;

import java.util.Date;

public class AAdmins
{
	/**
	 *    `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
		  `email` varchar(255) NOT NULL COMMENT '邮箱',
		  `password` varchar(255) NOT NULL COMMENT '密码',
		  `trueName` varchar(255) NOT NULL COMMENT '真实姓名',
		  `phone` varchar(255) NOT NULL COMMENT '手机号',
		  `qq` varchar(255) NOT NULL COMMENT 'qq',
		  `status` tinyint(4) NOT NULL COMMENT '状态:0:禁用;1:启用',
		  `createTime` datetime NOT NULL COMMENT '创建时间 ',
		  `updateTime` datetime NOT NULL COMMENT '更新时间',
		  `lastLoginTime` datetime NOT NULL COMMENT '上次登陆时间',
	 */
	
	private int id;
	private String email;
	private String password;
	private String trueName;
	private String phone;
	private String qq;
	private byte status;
	private Date createTime;
	private Date updateTime;
	private Date lastLoginTime;
	
	private String statusStr;
	
	public int getId()
	{
		return id;
	}
	public void setId(int id)
	{
		this.id = id;
	}
	public String getEmail()
	{
		return email;
	}
	public void setEmail(String email)
	{
		this.email = email;
	}
	public String getPassword()
	{
		return password;
	}
	public void setPassword(String password)
	{
		this.password = password;
	}
	public String getTrueName()
	{
		return trueName;
	}
	public void setTrueName(String trueName)
	{
		this.trueName = trueName;
	}
	public String getPhone()
	{
		return phone;
	}
	public void setPhone(String phone)
	{
		this.phone = phone;
	}
	public String getQq()
	{
		return qq;
	}
	public void setQq(String qq)
	{
		this.qq = qq;
	}
	public byte getStatus()
	{
		return status;
	}
	public void setStatus(byte status)
	{
		this.status = status;
	}
	public Date getCreateTime()
	{
		return createTime;
	}
	public void setCreateTime(Date createTime)
	{
		this.createTime = createTime;
	}
	public Date getUpdateTime()
	{
		return updateTime;
	}
	public void setUpdateTime(Date updateTime)
	{
		this.updateTime = updateTime;
	}
	public Date getLastLoginTime()
	{
		return lastLoginTime;
	}
	public void setLastLoginTime(Date lastLoginTime)
	{
		this.lastLoginTime = lastLoginTime;
	}
	@Override
	public String toString()
	{
		return "AAdmins [id=" + id + ", email=" + email + ", password=" + password + ", trueName=" + trueName
				+ ", phone=" + phone + ", qq=" + qq + ", status=" + status + ", createTime=" + createTime
				+ ", updateTime=" + updateTime + ", lastLoginTime=" + lastLoginTime + "]";
	}
	public String getStatusStr()
	{
		if(this.status == 0)
		{
			return "禁用";
		}
		if(this.status == 1)
		{
			return "启用";
		}
		return "未知";
		
	}
	
	
}
