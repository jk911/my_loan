package com.bjsxt.users.dao;

import com.bjsxt.common.dao.IBaseDao;
import com.bjsxt.users.pojo.AAdmins;

public interface IAAdminsDao extends IBaseDao<AAdmins>
{
	
}
