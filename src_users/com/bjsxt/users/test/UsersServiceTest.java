package com.bjsxt.users.test;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.alibaba.fastjson.JSONObject;
import com.bjsxt.common.test.BaseTest;
import com.bjsxt.common.util.ConstatFinalUtil;
import com.bjsxt.common.util.EncryptUtil;
import com.bjsxt.common.util.PageInfoUtil;
import com.bjsxt.users.pojo.AAdmins;
import com.bjsxt.users.service.IUsersService;

public class UsersServiceTest extends BaseTest
{	
	
	protected IUsersService  usersService;
	
	@Before
	public void init()
	{
		super.init();
		this.usersService = (IUsersService) this.ac.getBean("usersService");
	}
	@Test
	public void findOneAdminsService()
	{
		Map<String,Object> condMap = new HashMap<String,Object>();
		condMap.put("email", "11@11.com");
		AAdmins admins = this.usersService.findOneAdminsService(condMap);
		ConstatFinalUtil.LOGGER.info("管理员信息：{}",admins.getId() + "_" + admins.getEmail());
		
	}
	@Test
	public void insertOneAdminsService()
	{
		for (int i = 0; i < 50; i++)
		{
			EncryptUtil encryptUtil = new EncryptUtil();
			AAdmins admins  = new AAdmins();
			admins.setEmail("11@11.com");
			admins.setPassword(encryptUtil.encryString("1111"));
			admins.setTrueName("admin");
			admins.setPhone("11");
			admins.setQq("11");
			admins.setStatus(Byte.valueOf("1"));
			admins.setCreateTime(new Date());
			admins.setUpdateTime(new Date());
			admins.setLastLoginTime(new Date());
			JSONObject resultJSON = new JSONObject();
			resultJSON = this.usersService.insertOneAdminsService(admins);
			ConstatFinalUtil.LOGGER.info("添加管理员：{}",resultJSON);
		}
	}
	
	/**
	 * 查询多条记录
	 */
	@Test
	public void findCondListAdminsService()
	{
		PageInfoUtil pageInfoUtil = new PageInfoUtil();
		
		Map<String,Object> condMap = new HashMap<String, Object>();
		//condMap.put("keyword", "22");
		//condMap.put("status", "1");
		//condMap.put("stDate", new Date());
		//condMap.put("edDate", new Date());
		//List<AAdmins> adminsList = this.usersService.findCondListAdminsService(pageInfoUtil, condMap);
		List<AAdmins> adminsList = this.usersService.findCondListAdminsService(null, condMap);
		for (Iterator iterator = adminsList.iterator(); iterator.hasNext();)
		{
			AAdmins admins = (AAdmins) iterator.next();
			ConstatFinalUtil.LOGGER_SYSTEM.info("管理员:id:{},email:{}",admins.getId(),admins.getEmail());
		}
		
		ConstatFinalUtil.LOGGER_SYSTEM.info("分页信息:当前页:{},总条数:{},总页数:{}",pageInfoUtil.getCurrentPage(),
				pageInfoUtil.getTotalRecord(),pageInfoUtil.getTotalPage());
	}
}
