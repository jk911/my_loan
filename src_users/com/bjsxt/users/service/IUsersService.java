package com.bjsxt.users.service;

import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.bjsxt.common.util.PageInfoUtil;
import com.bjsxt.users.pojo.AAdmins;

/**
 * 所有users相关的service 接口
 * 包括（前台后台）
 * 
 * @author sec
 *
 */
public interface  IUsersService
{
	/*----  后台管理员操作开始    ----*/
	/**
	 * 添加一条管理员
	 * @param admins
	 * @return
	 */
	JSONObject  insertOneAdminsService(AAdmins admins);
	/*
	 * 更新一条数据
	 */
	JSONObject updateOneAdminsService(AAdmins admins);
	/**
	 * 查询一条管理员
	 * @param condMap
	 * @return
	 */
	AAdmins  findOneAdminsService(Map<String,Object> condMap);
	
	/**
	 * 查询多条管理员
	 * @param pageInfoUtil 分页对象
	 * @param condMap 条件n
	 * @return
	 */
	List<AAdmins> findCondListAdminsService(PageInfoUtil pageInfoUtil,Map<String,Object> condMap);
	/*----  后台管理员操作结束    ----*/
}
