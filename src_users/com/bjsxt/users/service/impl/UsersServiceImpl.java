package com.bjsxt.users.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.bjsxt.common.util.PageInfoUtil;
import com.bjsxt.users.dao.IAAdminsDao;
import com.bjsxt.users.pojo.AAdmins;
import com.bjsxt.users.service.IUsersService;
@Service("usersService")
public class UsersServiceImpl implements IUsersService
{
	@Resource
	private IAAdminsDao adminsDao;
	@Override
	public JSONObject insertOneAdminsService(AAdmins admins)
	{
		JSONObject resultJSON = new JSONObject();
		int res = this.adminsDao.insert(admins);
		if(res > 0)
		{
			resultJSON.put("code", 0);
			resultJSON.put("info", "添加成功");
			JSONObject dataJSON = new JSONObject();
			/*拿回主键*/
			dataJSON.put("id", admins.getId());
			resultJSON.put("data", dataJSON);
		}else
		{
			resultJSON.put("code", 1);
			resultJSON.put("info", "添加失败");
		}
		return resultJSON;
	}

	@Override
	public AAdmins findOneAdminsService(Map<String, Object> condMap)
	{
		// TODO Auto-generated method stub
		return this.adminsDao.findOne(condMap);
	}

	@Override
	public JSONObject updateOneAdminsService(AAdmins admins)
	{
		
		JSONObject resultJSON = new JSONObject();
		int res = this.adminsDao.update(admins);
		if(res > 0)
		{
			resultJSON.put("code", 0);
			resultJSON.put("info", "更新成功");
			JSONObject dataJSON = new JSONObject();
			
		}else
		{
			resultJSON.put("code", 1);
			resultJSON.put("info", "更新失败");
		}
		return resultJSON;
	}

	@Override
	public List<AAdmins> findCondListAdminsService(PageInfoUtil pageInfoUtil, Map<String, Object> condMap)
	{
		/*List<AAdmins> adminsList = new ArrayList<AAdmins>();
		*//**
		 * 处理关键字
		 * keyword 变成：% + keyword + %
		 *//*
		if(condMap.get("keyword") != null)
		{
			condMap.put("keyword", "%" + condMap.get("keyword") + "%");
		}
		pageCount == true:拿总记录数:
		pageCount == false:分页数据:
		pageCount 为null不分页查询
		if(pageInfoUtil != null)
		{
			分页查询
			先查询总记录数
			condMap.put("pageCount","true");
			adminsList = this.adminsDao.findCondList(condMap);
			 adminsList有一条记录,总记录数放到了id属性上 
			pageInfoUtil.setTotalRecord(adminsList.get(0).getId());
			拿分页数据
			condMap.put("pageCount", "false");
			 当前记录数
			 * 每页多少条 
			condMap.put("currentRecord", pageInfoUtil.getCurrRecord());
			condMap.put("pageSize", pageInfoUtil.getPageSize());
			adminsList = this.adminsDao.findCondList(condMap);
		}else
		{
			不分页查询
			return this.adminsDao.findCondList(condMap);
		}
		return adminsList;*/
		List<AAdmins> adminsList = new ArrayList<AAdmins>();
		/*
		 * 处理一下关键字
		 * keyword 变成: % + keyword + %
		 * */
		if(condMap.get("keyword") != null)
		{
			condMap.put("keyword", "%" + condMap.get("keyword") + "%");
		}
		
		/*pageCount == true:拿总记录数:
		pageCount == false:分页数据:
		pageCount 为null不分页查询*/
		if(pageInfoUtil != null)
		{
			//分页查询
			/* 先查询总记录数 */
			condMap.put("pageCount", "true");
			adminsList = this.adminsDao.findCondList(condMap);
			/* adminsList有一条记录,总记录数放到了id属性上 */
			pageInfoUtil.setTotalRecord(adminsList.get(0).getId());
			
			/* 拿分页数据 */
			condMap.put("pageCount", "false");
			/* 当前记录数
			 * 每页多少条 */
			condMap.put("currentRecord", pageInfoUtil.getCurrRecord());
			condMap.put("pageSize", pageInfoUtil.getPageSize());
			adminsList = this.adminsDao.findCondList(condMap);
		}else
		{
			//不分页查询
			adminsList = this.adminsDao.findCondList(condMap);
		}
		return adminsList;
	
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
