package com.bjsxt.back.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bjsxt.common.controller.BaseController;
import com.bjsxt.common.util.ConstatFinalUtil;
import com.bjsxt.common.util.PageInfoUtil;
import com.bjsxt.system.pojo.ARegion;
import com.bjsxt.system.service.ISystemService;


/**
 * 系统相关的controller
 * 
 * 
 * @author sec
 *
 */

@Controller
@RequestMapping("/back/system/")
public class SystemBackController extends BaseController
{	
	@Resource
	private ISystemService systemService;
	/**
	 * 地区列表
	 */
	@RequestMapping("/regionList.htm")
	public String regionList(HttpServletRequest request)
	{
		ConstatFinalUtil.LOGGER.info("----------------regionList--------------------");
		/**
		 *接受参数
		 */
		String keyword = request.getParameter("keyword");
		String status = request.getParameter("status");
		String st = request.getParameter("st");
		String ed = request.getParameter("ed");
		String parentId = request.getParameter("parentId");
		if(st == null)
		{
			st = "" ; 
		}
		if(ed == null)
		{
			ed = "";
		}
		if(parentId == null)
		{
			parentId = "";
		}
		Date stDate = null;
		Date edDate = null;
		if(!"".equalsIgnoreCase(st) && !"".equalsIgnoreCase(ed))
		{
			stDate = this.dateFormatUtil.parseDateTime(st);
			edDate = this.dateFormatUtil.parseDateTime(ed);
		}
		PageInfoUtil pageInfoUtil = this.proccedPageInfo(request);
		Map<String,Object> condMap = new HashMap<String,Object>();
		condMap.put("keyword", keyword);
		condMap.put("status", status);
		condMap.put("stDate", stDate);
		condMap.put("edDate", edDate);
		condMap.put("parentId", parentId);
		
		List<ARegion> regionList = this.systemService.findCondListRegionService(pageInfoUtil, condMap);
		/**
		 * 将参数放在作用于中
		 */
		request.setAttribute("regionList", regionList);
		request.setAttribute("pageInfoUtil", pageInfoUtil);
		request.setAttribute("keyword", keyword);
		request.setAttribute("status", status);
		request.setAttribute("st", st);
		request.setAttribute("ed", ed);
		request.setAttribute("parentId", parentId);
		return "/back/system/regionList";
	}
	
	
	
	
}
