package com.bjsxt.back.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bjsxt.common.controller.BaseController;
import com.bjsxt.common.util.ConstatFinalUtil;
import com.bjsxt.common.util.PageInfoUtil;
import com.bjsxt.investor.pojo.AInvestor;
import com.bjsxt.investor.service.IInvestorService;
@Controller
@RequestMapping("/back/investor/")
public class InvestorBackController extends BaseController
{
	@Resource
	private IInvestorService investorService;
	/**
	 *  出资方列表
	 * @return
	 */
	@RequestMapping("/investorList.htm")
	public String investorList(HttpServletRequest request)
	{
		
		ConstatFinalUtil.LOGGER.info("-------investorList-------");
		/*接受参数*/
		String keyword = request.getParameter("keyword");
		String status = request.getParameter("status");
		String st = request.getParameter("st");
		String ed = request.getParameter("ed");
		String coopType = request.getParameter("coopType");
		if(st == null)
		{
			st = "";
		}
		if(ed == null)
		{
			ed ="";
		}
		Date stDate = null;
		Date edDate = null;
		if(!"".equalsIgnoreCase(st)&& !"".equalsIgnoreCase(ed))
		{
			stDate = this.dateFormatUtil.parseDateTime(st);
			edDate = this.dateFormatUtil.parseDateTime(ed);
		}
		PageInfoUtil pageInfoUtil = this.proccedPageInfo(request);
		Map<String,Object> condMap = new HashMap<String,Object>();
		condMap.put("keyword", keyword);
		condMap.put("status", status);
		condMap.put("stDate", stDate);
		condMap.put("edDate", edDate);
		condMap.put("coopType", coopType);
		List<AInvestor> investorList = this.investorService.findCondListInvestorService(pageInfoUtil, condMap);
		/**
		 * 将前台的结果数据放在request中
		 */
		request.setAttribute("investorList", investorList);
		request.setAttribute("pageinfoUtil", pageInfoUtil);
		/**
		 * 将条件存放在request中
		 * 将条件乡下传递
		 */
		request.setAttribute("keyword", keyword);
		request.setAttribute("status", status);
		request.setAttribute("st", st);
		request.setAttribute("ed", ed);
		request.setAttribute("coopType", coopType);
		
		return "/back/investor/investorList";
		
	}
}
