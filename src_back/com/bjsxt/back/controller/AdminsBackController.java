package com.bjsxt.back.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.bjsxt.common.controller.BaseController;
import com.bjsxt.common.util.ConstatFinalUtil;
import com.bjsxt.common.util.PageInfoUtil;
import com.bjsxt.users.pojo.AAdmins;
import com.bjsxt.users.service.IUsersService;
@Controller
@RequestMapping("/back/admins/")
public class AdminsBackController extends BaseController
{

	@Resource
	private IUsersService usersService;
	/**
	 * 登陆后首页
	 * @return
	 */
	@RequestMapping("/index.htm")
	public String index()
	{
		ConstatFinalUtil.LOGGER_SYSTEM.info("------AdminsBackController-----");
		return "/back/index";
		
	}
	/**
	 * 登陆后左侧页面
	 * @return
	 */
	@RequestMapping("/leftMenu.htm")
	public String leftMenu()
	{
		ConstatFinalUtil.LOGGER_SYSTEM.info("----------------leftMenu---------------");
		return "/back/leftMenu";
	}
	@RequestMapping("/adminsList.htm")
	public String adminsList(HttpServletRequest request)
	{
		ConstatFinalUtil.LOGGER_SYSTEM.info("--------- adminsList ------------");
		/*查询条件（对象属性赋值？？）
		String keyword = request.getParameter("keyword");
		String status = request.getParameter("status");
		String st = request.getParameter("st");
		String ed = request.getParameter("ed");
		
		处理参数adminsList
		if(st == null)
		{
			st = "";
		}
		if(ed == null)
		{
			ed ="";
		}
		初始化stDate和edDate
		 * 存放格式化以后的日期
		 * 
		Date stDate = null;
		Date edDate = null;
		if(!"".equalsIgnoreCase(st) && !"".equalsIgnoreCase(ed))
		{
			stDate = this.dateFormatUtil.parseDateTime(st);
			edDate = this.dateFormatUtil.parseDateTime(ed);
		}
		处理分页信息
		PageInfoUtil pageInfoUtil = this.proccedPageInfo(request);
		查询数据
		Map<String,Object> condMap = new HashMap<String,Object>();
		condMap.put("keyword", keyword);
		condMap.put("status", status);
		condMap.put("stDate", stDate);
		condMap.put("edDate", edDate);
		List<AAdmins>  adminsList = this.usersService.findCondListAdminsService(pageInfoUtil, condMap);
		request.setAttribute("adminsList",adminsList);
		request.setAttribute("pageInfoUtil", pageInfoUtil);
		存储条件
		request.setAttribute("keyword", keyword);
		request.setAttribute("status", status);
		request.setAttribute("st", st);
		request.setAttribute("ed", ed);*/
		
		/* 接收参数 */
		String keyword = request.getParameter("keyword");
		String status = request.getParameter("status");
		String st = request.getParameter("st");
		String ed = request.getParameter("ed");
		if(st == null)
		{
			st = "" ; 
		}
		if(ed == null)
		{
			ed = "" ; 
		}
		
		Date stDate = null ; 
		Date edDate = null ; 
		if(!"".equalsIgnoreCase(st) && !"".equalsIgnoreCase(ed))
		{
			stDate = this.dateFormatUtil.parseDateTime(st);
			edDate = this.dateFormatUtil.parseDateTime(ed);
		}
		
		PageInfoUtil pageInfoUtil = this.proccedPageInfo(request);
		/* 查询数据 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("keyword", keyword);
		condMap.put("status", status);
		condMap.put("stDate", stDate);
		condMap.put("edDate", edDate);
		List<AAdmins> adminsList = this.usersService.findCondListAdminsService(pageInfoUtil, condMap);
		/* 存储结果 */
		request.setAttribute("adminsList", adminsList);
		request.setAttribute("pageInfoUtil", pageInfoUtil);
		
		/* 存储条件 */
		request.setAttribute("keyword", keyword);
		request.setAttribute("status", status);
		request.setAttribute("st", st);
		request.setAttribute("ed", ed);
		
		return "/back/adminsList";
	}
	
	
	/**
	 * 打开管理员添加页面
	 * @return
	 */
	@RequestMapping("/adminsInsert.htm")
	public String adminsInsert()
	{
		ConstatFinalUtil.LOGGER_SYSTEM.info("--- adminsInsert ---");
		return "/back/adminsInsert";
	}
	/**
	 * 打开admins提交页面
	 * produces="text/html;charset=UTF-8"==
	 * response.setContextType("text/html;charset=UTF-8");
	 * @return
	 */
	@RequestMapping(value="/adminsInsertSubmit.htm",produces="text/html;charset=UTF-8")
	public @ResponseBody String adminsInsertSubmit(AAdmins admins,HttpServletRequest request)
	{
		admins.setPassword(this.entryptUtil.encryString(admins.getPassword()));
		admins.setCreateTime(new Date());
		admins.setUpdateTime(new Date());
		admins.setLastLoginTime(new Date());
		JSONObject resultJSON = this.usersService.insertOneAdminsService(admins);
		ConstatFinalUtil.LOGGER_SYSTEM.info("---------",resultJSON);
		resultJSON = this.returnJuiJSON(request,resultJSON.getString("info"));
		/* 返回JUI需要的JSON
		 * @ResponseBody:默认返回字符串
		 *  */
		return resultJSON.toJSONString();
		
	}
	
	
/*
	*//**
	 * 登陆后左侧页面
	 * @return
	 *//*
	@RequestMapping("/leftMenu.htm")
	public String leftMenu()
	{
		ConstatFinalUtil.LOGGER_SYSTEM.info("----------------leftMenu---------------");
		return "/back/leftMenu";
	}
	@RequestMapping("/adminsList.htm")
	public String adminsList(HttpServletRequest request)
	{
		ConstatFinalUtil.LOGGER_SYSTEM.info("--------- adminsList ------------");
		查询条件（对象属性赋值？？）
		String keyword = request.getParameter("keyword");
		String status = request.getParameter("status");
		String st = request.getParameter("st");
		String ed = request.getParameter("ed");
		
		处理参数adminsList
		if(st == null)
		{
			st = "";
		}
		if(ed == null)
		{
			ed ="";
		}
		初始化stDate和edDate
		 * 存放格式化以后的日期
		 * 
		Date stDate = null;
		Date edDate = null;
		if(!"".equalsIgnoreCase(st) && !"".equalsIgnoreCase(ed))
		{
			stDate = this.dateFormatUtil.parseDateTime(st);
			edDate = this.dateFormatUtil.parseDateTime(ed);
		}
		处理分页信息
		PageInfoUtil pageInfoUtil = this.proccedPageInfo(request);
		查询数据
		Map<String,Object> condMap = new HashMap<String,Object>();
		condMap.put("keyword", keyword);
		condMap.put("status", status);
		condMap.put("stDate", stDate);
		condMap.put("edDate", edDate);
		List<AAdmins>  adminsList = this.usersService.findCondListAdminsService(pageInfoUtil, condMap);
		request.setAttribute("adminsList",adminsList);
		request.setAttribute("pageInfoUtil", pageInfoUtil);
		存储条件
		request.setAttribute("keyword", keyword);
		request.setAttribute("status", status);
		request.setAttribute("st", st);
		request.setAttribute("ed", ed);
		
		 接收参数 
		String keyword = request.getParameter("keyword");
		String status = request.getParameter("status");
		String st = request.getParameter("st");
		String ed = request.getParameter("ed");
		if(st == null)
		{
			st = "" ; 
		}
		if(ed == null)
		{
			ed = "" ; 
		}
		
		Date stDate = null ; 
		Date edDate = null ; 
		if(!"".equalsIgnoreCase(st) && !"".equalsIgnoreCase(ed))
		{
			stDate = this.dateFormatUtil.parseDateTime(st);
			edDate = this.dateFormatUtil.parseDateTime(ed);
		}
		
		PageInfoUtil pageInfoUtil = this.proccedPageInfo(request);
		 查询数据 
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("keyword", keyword);
		condMap.put("status", status);
		condMap.put("stDate", stDate);
		condMap.put("edDate", edDate);
		List<AAdmins> adminsList = this.usersService.findCondListAdminsService(pageInfoUtil, condMap);
		 存储结果 
		request.setAttribute("adminsList", adminsList);
		request.setAttribute("pageInfoUtil", pageInfoUtil);
		
		 存储条件 
		request.setAttribute("keyword", keyword);
		request.setAttribute("status", status);
		request.setAttribute("st", st);
		request.setAttribute("ed", ed);
		
		return "/back/adminsList";
	}
	
	
	登陆后退出
	@RequestMapping("/logout.htm")
	public String logout(HttpServletRequest request)
	{
		*//**
		 * 将登陆时放入session的登录信息remove
		 *//*
		HttpSession session = request.getSession();
		session.removeAttribute("admins");
		session.removeAttribute("lastLoginTime");
		return "redirect:/adminsLogin.htm" ; 
	}
	*/
	
}
