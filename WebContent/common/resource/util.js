/**
 * JS中全局的变量
 */
var rootpath = "/my_loan";
/**
 * 刷新验证码
 * 
 * ~获取验证码的jquery对象
 * ~刷新请求(给验证码的src重新赋值)
 * 
 * @param imgCode:验证码图片的id
 * @returns
 */

function refreshCode(imgCode)
{
	var imgCodeJq = $("#"+imgCode);
	imgCodeJq.attr("src",rootpath + "/code.htm?now=" + new Date());
	/*alert(1);*/
	return false;
}

/**
 * ~获取DOM元素(Jquery元素)
 * ~将参数中的值赋值给表单中的文本框(当前页),(每页多少条)
 * ~提交表单
 * @returns
 */
function pageFormSubmit(formId,currentPageId,currentPage,pageSizeId,pageSize)
{
	/* 获取DOM元素(Jquery元素)
	 * 将参数中的值赋值给表单中的文本框(当前页),(每页多少条)
	 */
	$("#" + currentPageId).val(currentPage);
	$("#" + pageSizeId).val(pageSize);
	
	$("#" + formId).submit();
	return false ;
}
