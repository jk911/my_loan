<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<form id="pagerForm" method="post" action="demo_page1.html">
	<input type="hidden" name="status" value="${param.status}"> <input
		type="hidden" name="keywords" value="${param.keywords}" /> <input
		type="hidden" name="pageNum" value="1" /> <input type="hidden"
		name="numPerPage" value="${model.numPerPage}" /> <input type="hidden"
		name="orderField" value="${param.orderField}" />
</form>


<div class="pageHeader">
	<form onsubmit="return navTabSearch(this);" action="${rootpath }/back/investor/investorList.htm" method="post">
			
		<div class="searchBar">
			<table class="searchContent">
				<tr>
					<td>
						关键字：<input type="text" name="keyword" value="${requestScope.keyword }"/>
						状态:
						<select name="status">
							<option value="">请选择</option>
							<option value="1" ${requestScope.status == '1' ? 'selected' : '' }>启用</option>
							<option value="0" ${requestScope.status == '0' ? 'selected' : '' }>禁用</option>
						</select>
						创建时间：
						<input type="text" class="date" name="st" readonly="true" datefmt="yyyy-MM-dd HH:mm:ss" value="${requestScope.st }"/>-->
						<input type="text" class="date" name="ed" readonly="true" datefmt="yyyy-MM-dd HH:mm:ss" value="${requestScope.ed }"/>
					</td>
				</tr>
			</table>
			<div class="subBar">
				<ul>
					<li><div class="buttonActive">
							<div class="buttonContent">
								<button type="submit">检索</button>
							</div>
						</div></li>
					<li><a class="button" href="demo_page6.html" target="dialog"
						mask="true" title="查询框"><span>高级检索</span></a></li>
				</ul>
			</div>
		</div>
	</form>
</div>
<div class="pageContent">
	<div class="panelBar">
		<ul class="toolBar">
			<li><a class="add" href="${rootpath }/back/investor/investorInsert.htm" target="navTab" rel="investorInsert"><span>添加</span></a></li>
			<li><a class="delete"
				href="demo/common/ajaxDone.html?uid={sid_user}" target="ajaxTodo"
				title="确定要删除吗?"><span>删除</span></a></li>
			<li><a class="edit" href="demo_page4.html?uid={sid_user}"
				target="navTab"><span>修改</span></a></li>
			<li class="line">line</li>
			<li><a class="icon" href="demo/common/dwz-team.xls"
				target="dwzExport" targetType="navTab" title="实要导出这些记录吗?"><span>导出EXCEL</span></a></li>
		</ul>
	</div>
	<table class="table" width="100%" layoutH="138">
		<thead>
			<tr>
				<th width="80">序号</th>
				<th width="80">名称</th>
				<th width="100">编码</th>
				<th width="100">url</th>
				<th width="100">联系人</th>
				<th width="100">联系电话</th>
				<th width="100">总量</th>
				<th width="100">销量</th>
				<th width="100">类型</th>
				<th width="80">状态</th>
				<th width="120">创建时间</th>
				<th width="120">更新时间</th>
				<th width="120">发布时间</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${requestScope.investorList }" var="investor" varStatus="stat">
				<tr target="sid_user" rel="${invertor.id }">
					<td>${stat.count }</td>
					<td>${investor.name }</td>
					<td>${investor.code }</td>
					<td>${investor.url }</td>
					<td>${investor.contactName }</td>
					<td>${investor.contactPhone }</td>
					<td>${investor.totalNum }</td>
					<td>${investor.succedNum }</td>
					<td>${investor.coopTypeStr }</td>
					<td>${investor.statusStr }</td>
					<td><fmt:formatDate value="${investor.createTime }" pattern="yyyy-MM-dd HH:mm:ss"/></td>
					<td><fmt:formatDate value="${investor.updateTime }" pattern="yyyy-MM-dd HH:mm:ss"/></td>
					<td><fmt:formatDate value="${investor.pubTime }" pattern="yyyy-MM-dd HH:mm:ss"/></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<div class="panelBar">
		<form action="${rootpath }/back/investor/investorList.htm" method="post" id="investorListPageForm" onsubmit="return navTabSearch(this);">
			<input type="hidden" name="keyword" value="${requestScope.keyword }">
			<input type="hidden" name="status" value="${requestScope.status }">
			<input type="hidden" name="st" value="${requestScope.st }">
			<input type="hidden" name="ed" value="${requestScope.ed }">
			<div class="pages">
				<a href="" class="button" onclick="return pageFormSubmit('investorListPageForm','investorListCurrentPage','1','investorListPageSize','${requestScope.pageInfoUtil.pageSize }')"><span>首页</span>&nbsp;</a>
				<a href="" class="button" onclick="return pageFormSubmit('investorListPageForm','investorListCurrentPage','${requestScope.pageInfoUtil.prePage }','investorListPageSize','${requestScope.pageInfoUtil.pageSize }')"><span>上一页</span>&nbsp;</a>
				<a href="" class="button" onclick="return pageFormSubmit('investorListPageForm','investorListCurrentPage','${requestScope.pageInfoUtil.nextPage }','investorListPageSize','${requestScope.pageInfoUtil.pageSize }')"><span>下一页</span>&nbsp;</a>
				<a href="" class="button" onclick="return pageFormSubmit('investorListPageForm','investorListCurrentPage','${requestScope.pageInfoUtil.totalPage }','investorListPageSize','${requestScope.pageInfoUtil.pageSize }')"><span>尾页</span>&nbsp;</a>
				共${requestScope.pageInfoUtil.currentPage }/${requestScope.pageInfoUtil.totalPage }页
				共${requestScope.pageInfoUtil.totalRecord }条
				<input type="text" name="currentPage" id="investorListCurrentPage" value="${requestScope.pageInfoUtil.currentPage }" size="5" maxlength="5">
				<input type="text" name="pageSize" id="investorListPageSize" value="${requestScope.pageInfoUtil.pageSize }" size="5" maxlength="5">
				<input type="submit" value="GO">
			</div>
		</form>
	</div>
</div>
