<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>小额贷款平台</title>
<link href="${rootpath }/dwz_jui/themes/css/login.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${rootpath }/common/resource/jquery-3.0.0.min.js"></script>
<script type="text/javascript" src="${rootpath }/common/resource/util.js"></script>
</head>

<body>
	<div id="login">
		<div id="login_header">
			<h1 class="login_logo">
				<a href="http://demo.dwzjs.com"><img src="${rootpath }/dwz_jui/themes/default/images/login_logo.gif" /></a>
			</h1>
			<div class="login_headerContent">
				<div class="navList">
					<ul>
						<li><a href="#">设为首页</a></li>
						<li><a href="http://bbs.dwzjs.com">反馈</a></li>
						<li><a href="doc/dwz-user-guide.pdf" target="_blank">帮助</a></li>
					</ul>
				</div>
				<h2 class="login_title"><img src="${rootpath }/dwz_jui/themes/default/images/login_title.png" /></h2>
			</div>
		</div>
		<div id="login_content">
			<div class="loginForm">
				<form action="${rootpath }/adminsLoginSubmit.htm" method="post">
					<p>
						<label>用户名：</label>
						<input type="text" name="email" size="20" class="login_input" />
					</p>
					<p>
						<label>密码：</label>
						<input type="password" name="password" size="20" class="login_input" />
					</p>
					<p>
						<label>验证码：</label>
						<input class="code" type="text" size="5" name="code"/>
						<span>
							<a href="#" onclick="return refreshCode('imgCode')" >
								<img src="${rootpath }/code.htm" id="imgCode" alt="" width="75" height="24" />
							</a>
						</span>
					</p>
					<div class="login_bar">
						<input class="sub" type="submit" value=" " />
					</div>
					<div>${info }</div>
				</form>
			</div>
			<div class="login_banner"><img src="${rootpath }/dwz_jui/themes/default/images/login_banner.jpg" /></div>
			<div class="login_main">
				
				
			</div>
		</div>
		<div id="login_footer">
			Copyright &copy; 呵呵哒
		</div>
	</div>
</body>
</html>