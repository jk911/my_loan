<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<form id="pagerForm" method="post" action="demo_page1.html">
	<input type="hidden" name="status" value="${param.status}"> <input
		type="hidden" name="keywords" value="${param.keywords}" /> <input
		type="hidden" name="pageNum" value="1" /> <input type="hidden"
		name="numPerPage" value="${model.numPerPage}" /> <input type="hidden"
		name="orderField" value="${param.orderField}" />
</form>


<div class="pageHeader">
	<form onsubmit="return navTabSearch(this);" action="${rootpath }/back/system/regionList.htm" method="post">
		<div class="searchBar">
			<table class="searchContent">
				<tr>
					<td>
						关键字：<input type="text" name="keyword" value="${requestScope.keyword }"/>
						状态:
						<select name="status">
							<option value="">请选择</option>
							<option value="1" ${requestScope.status == '1' ? 'selected' : '' }>启用</option>
							<option value="0" ${requestScope.status == '0' ? 'selected' : '' }>禁用</option>
						</select>
						创建时间：
						<input type="text" class="date" name="st" readonly="true" datefmt="yyyy-MM-dd HH:mm:ss" value="${requestScope.st }"/>-->
						<input type="text" class="date" name="ed" readonly="true" datefmt="yyyy-MM-dd HH:mm:ss" value="${requestScope.ed }"/>
					</td>
				</tr>
			</table>
			<div class="subBar">
				<ul>
					<li><div class="buttonActive">
							<div class="buttonContent">
								<button type="submit">检索</button>
							</div>
						</div></li>
					<li><a class="button" href="demo_page6.html" target="dialog"
						mask="true" title="查询框"><span>高级检索</span></a></li>
				</ul>
			</div>
		</div>
	</form>
</div>
<div class="pageContent">
	<div class="panelBar">
		<ul class="toolBar">
			<li><a class="add" href="${rootpath }/back/system/regionInsert.htm" target="navTab" rel="regionInsert"><span>添加</span></a></li>
			<li><a class="delete"
				href="demo/common/ajaxDone.html?uid={sid_user}" target="ajaxTodo"
				title="确定要删除吗?"><span>删除</span></a></li>
			<li><a class="edit" href="demo_page4.html?uid={sid_user}"
				target="navTab"><span>修改</span></a></li>
			<li><a class="edit" href="${rootpath }/back/system/regionList.htm?parentId=0" target="navTab" rel="regionList"><span>查看所有省份</span></a></li>
			<li><a class="edit" href="${rootpath }/back/system/regionList.htm?parentId={sid_user}" target="navTab" rel="regionList"><span>查看子节点</span></a></li>
			<li><a class="icon" href="demo/common/dwz-team.xls"
				target="dwzExport" targetType="navTab" title="实要导出这些记录吗?"><span>导出EXCEL</span></a></li>
		</ul>
	</div>
	<table class="table" width="100%" layoutH="138">
		<thead>
			<tr>
				<th width="80">序号</th>
				<th width="80">父名称</th>
				<th width="80">名称</th>
				<th width="100">拼音</th>
				<th width="100">areaCode</th>
				<th width="80">叶子节点</th>
				<th width="80">状态</th>
				<th width="120">创建时间</th>
				<th width="120">更新时间</th>
				<th width="120">上次登陆时间</th>
			</tr>
		</thead>
		<tbody>
			<!-- 循环拿到对象 -->
			<c:forEach items="${requestScope.regionList }" var="region" varStatus="stat">
				<tr target="sid_user" rel="${region.id }">
					<th width="80">${stat.count}</th>
					<th width="80">${region.parentRegion.name }</th>
					<th width="80">${region.name }</th>
					<th width="100">${region.pinyin }</th>
					<th width="100">${region.areacode }</th>
					<th width="80">${region.leafStatusStr }</th>
					<th width="80">${region.status }</th>
					<th width="120"><fmt:formatDate value="${region.createTime }" pattern="yyyy-MM-dd HH:mm:ss"/></th>
					<th width="120"><fmt:formatDate value="${region.updateTime }" pattern="yyyy-MM-dd HH:mm:ss"/></th>
					<th width="120"><fmt:formatDate value="${region.pubTime }" pattern="yyyy-MM-dd HH:mm:ss"/></th>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<div class="panelBar">
		<form action="${rootpath }/back/system/regionList.htm" method="post" id="regionListPageForm" onsubmit="return navTabSearch(this);">
			<input type="hidden" name="keyword" value="${requestScope.keyword }">
			<input type="hidden" name="status" value="${requestScope.status }">
			<input type="hidden" name="st" value="${requestScope.st }">
			<input type="hidden" name="ed" value="${requestScope.ed }">
			<input type="hidden" name="parentId" value="${request.parentId }">
			<div class="pages">
				<a href="" class="button" onclick="return pageFormSubmit('regionListPageForm','regionListCurrentPage','1','regionListPageSize','${requestScope.pageInfoUtil.pageSize }')"><span>首页</span>&nbsp;</a>
				<a href="" class="button" onclick="return pageFormSubmit('regionListPageForm','regionListCurrentPage','${requestScope.pageInfoUtil.prePage }','regionListPageSize','${requestScope.pageInfoUtil.pageSize }')"><span>上一页</span>&nbsp;</a>
				<a href="" class="button" onclick="return pageFormSubmit('regionListPageForm','regionListCurrentPage','${requestScope.pageInfoUtil.nextPage }','regionListPageSize','${requestScope.pageInfoUtil.pageSize }')"><span>下一页</span>&nbsp;</a>
				<a href="" class="button" onclick="return pageFormSubmit('regionListPageForm','regionListCurrentPage','${requestScope.pageInfoUtil.totalPage }','regionListPageSize','${requestScope.pageInfoUtil.pageSize }')"><span>尾页</span>&nbsp;</a>
				共${requestScope.pageInfoUtil.currentPage }/${requestScope.pageInfoUtil.totalPage }页
				共${requestScope.pageInfoUtil.totalRecord }条
				<input type="text" name="currentPage" id="regionListCurrentPage" value="${requestScope.pageInfoUtil.currentPage }" size="5" maxlength="5">
				<input type="text" name="pageSize" id="regionListPageSize" value="${requestScope.pageInfoUtil.pageSize }" size="5" maxlength="5">
				<input type="submit" value="GO">
			</div>
		</form>
	</div>
</div>