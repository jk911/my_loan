<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp" %>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<h2 class="contentTitle">管理员添加</h2>
<div class="pageContent">
	<form method="post" action="${rootpath }/back/admins/adminsInsertSubmit.htm"
		class="pageForm required-validate"
		onsubmit="return validateCallback(this)">
		<div class="pageFormContent nowrap" layoutH="97">
			<dl>
				<dt>邮箱：</dt>
				<dd>
					<input type="text" name="email" class="required email"
						alt="请输入您的电子邮件" />
					<span class="info"></span>
				</dd>
			</dl>
			<dl>
				<dt>密码：</dt>
				<dd>
					<input id="adminsInsertPassword" type="password" name="password"
						class="required alphanumeric" minlength="6" maxlength="20"
						alt="字母、数字、下划线 6-20位" />
					<span class="info"></span>
				</dd>
			</dl>
			<dl>
				<dt>确认密码：</dt>
				<dd>
					<input type="password" name="repassword" class="required"
						equalto="#adminsInsertPassword" />
					<span class="info"></span>
				</dd>
			</dl>
			<dl>
				<dt>真实姓名：</dt>
				<dd>
					<input type="text" name="trueName" class="required" alt="请输入您的真实姓名" />
					<span class="info"></span>
				</dd>
			</dl>
			<dl>
				<dt>电话：</dt>
				<dd>
					<input type="text" name="phone" class="phone" alt="请输入您的电话" />
					<span class="info"></span>
				</dd>
			</dl>
			<dl>
				<dt>qq：</dt>
				<dd>
					<input type="text" name="qq" class="digits" alt="请输入您的qq" />
					<span class="info"></span>
				</dd>
			</dl>
			<dl>
				<dt>状态：</dt>
				<dd>
					<input type="radio" id="status1AdminsInsert" name="status" value="1" checked="checked"><label class="label_none" for="status1AdminsInsert">启用</label>
					<input type="radio" id="status0AdminsInsert" name="status" value="0"><label class="label_none" for="status0AdminsInsert">禁用</label>
				</dd>
			</dl>
			<!-- <div class="divider"></div>
			<p>自定义扩展请参照：dwz.validate.method.js</p>
			<p>错误提示信息国际化请参照：dwz.regional.zh.js</p> -->
		</div>
		<div class="formBar">
			<ul>
				<li><div class="buttonActive">
						<div class="buttonContent">
							<button type="submit">提交</button>
						</div>
					</div></li>
				<li><div class="button">
						<div class="buttonContent">
							<button type="button" class="close">取消</button>
						</div>
					</div></li>
			</ul>
		</div>
	</form>
</div>

<script type="text/javascript">
	function customvalidXxx(element) {
		if ($(element).val() == "xxx")
			return false;
		return true;
	}
</script>