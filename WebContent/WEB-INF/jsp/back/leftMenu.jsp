<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<div class="accordion" fillSpace="sidebar">
	<!-- <div class="accordionHeader">
		<h2>
			<span>Folder</span>界面组件
		</h2>
	</div> -->
	<div class="accordionContent">
		<ul class="tree treeFolder">
			<c:choose>
				<c:when test="${param.oper == 'userMana' }">
					<!-- 用户管理 -->
					<li>
						<a href="tabsPage.html" target="navTab">用户管理</a>
						<ul>
							<li>
								<a href="main.html" target="navTab" rel="main">用户列表</a>
							</li>
						</ul>
					</li>
				</c:when>
				<c:when test="${param.oper == 'invMana' }">
					<%--
						银行管理
						授权管理
					 --%>
					<!-- 出资方管理 -->
					<li>
						<a href="tabsPage.html" target="navTab">出资方管理</a>
						<ul>
							<li>
								<a href="${rootpath }/back/investor//investorList.htm" target="navTab" rel="investorList">出资方列表</a>
							</li>
						</ul>
					</li>
				</c:when>
				<c:when test="${param.oper == 'sysMana' }">
					<!-- 系统管理 -->
					<!-- 用户管理 -->
					<li>
						<a href="tabsPage.html" target="navTab">管理员管理</a>
						<ul>
							<li>
								<a href="${rootpath }/back/admins/adminsList.htm" target="navTab" rel="adminsList">管理员列表</a>
							</li>

							<li>
								<a href="${rootpath }/back/admins/adminsInsert.htm" target="navTab" rel="adminsInsert">管理员添加列表</a>
							</li>

						</ul>
					</li>
					<li>
						<a href="${rootpath }/back/system/regionList.htm" target="navTab">地区管理</a>
						<ul>
							<li>
								<a href="${rootpath }/back/system/regionList.htm" target="navTab" rel="regionList">地区列表</a>
							</li>
							<%-- <li>
								<a href="${rootpath }/back/admins/adminsInsert.htm" target="navTab" rel="adminsInsert">管理员添加列表</a>
							</li> --%>
						</ul>
					</li>
				</c:when>
			</c:choose>
		</ul>
	</div>
</div>